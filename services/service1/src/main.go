package main

import (
	"fmt"
	"net/http"
	"os"
)

func main() {
	port := os.Getenv("HTTP_PORT")
	if port == "" {
		port = "8080"
	}

	handler := http.NewServeMux()
	handler.HandleFunc("/service1", SayHello)
	handler.HandleFunc("/health", health)

	http.ListenAndServe("0.0.0.0:"+port, handler)
}

func SayHello(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, `Hello from service1`)
}

func health(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusAccepted)
	fmt.Fprintf(w, "OK")
}
