const express = require('express')
const app = express()

app.get('/service2', (req, res) => res.send('Hello from service2'))
app.get('/health', (req, res) => res.send('OK'))
app.listen(8080, () => console.log('Server ready'))
