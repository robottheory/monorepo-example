# monorepo-example
This is an example of a polyglot microservice monorepo that is designed for CD using Google Cloud.  The `services` directory contains two example services, one in golang and one in javascript.

Each service directory contains both the source and the helm chart that is used to deploy the service in kubernetes.  The helm templates are in the `templates` directory.  This includes yaml files for each of the components (service, deployment, etc) that need to be applied in k8s to make the service functional.

Each service also includes a Dockerfile that is used to build the image that will be deployed using the aforementioned templates.

The build and deploy process is controlled by the `cloudbuild.yaml` file.  You can either run this manually using `gcloud builds submit...` or by triggering it with one of the automations.  (Automatic triggering via merge to the repo is expected to be the typical method.)

One note: this also shows how to build a minimal container using the builder pattern.  The golang image is about 4.5MB in size.  Since javascript is an interpreted language, it can't be built on a scratch container, so the best we can get is about 45MB, or ten times the size.
